package com.tecsup.petclinic.service;

import com.tecsup.petclinic.domain.Vets;
import com.tecsup.petclinic.exception.VetsNotFoundException;

public interface VetsService {
	
	Vets findById(long id) throws VetsNotFoundException;
	
	Vets create(Vets vets);
	
	Vets update(Vets vets);
	
	void delete(long id) throws VetsNotFoundException;
	
	

}
