package com.tecsup.petclinic.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tecsup.petclinic.domain.Vets;
import com.tecsup.petclinic.domain.VetsRepository;
import com.tecsup.petclinic.exception.VetsNotFoundException;

@Service
public class VetsServiceImpl implements VetsService {

	@Autowired
	private VetsRepository vetsRepository;
	
	@Override
	public Vets findById(long id) throws VetsNotFoundException {
		Optional<Vets> vets = vetsRepository.findById(id);
		if ( !vets.isPresent())
			throw new VetsNotFoundException("Record not found...!");
		return vets.get();
	}

	@Override
	public Vets create(Vets vets) {
		return vetsRepository.save(vets);
	}

	@Override
	public Vets update(Vets vets) {
		return vetsRepository.save(vets);
	}

	@Override
	public void delete(long id) throws VetsNotFoundException {
		Vets vets=findById(id);
		vetsRepository.delete(vets);
	}
}
