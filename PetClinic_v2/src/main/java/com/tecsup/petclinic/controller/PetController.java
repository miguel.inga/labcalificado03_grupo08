package com.tecsup.petclinic.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.tecsup.petclinic.domain.Owner;
import com.tecsup.petclinic.exception.OwnerNotFoundException;
import com.tecsup.petclinic.service.OwnerService;

@RestController
public class PetController {
	
	@Autowired
	private OwnerService ownerService;
	
	@GetMapping("/owners")
	Iterable<Owner> all() {
	    return ownerService.findAll();
	}
	
	
	@GetMapping("/owners/id/{id}")
	Owner findById(@PathVariable Long id) throws OwnerNotFoundException {
	    return ownerService.findById(id);
	}

	@PostMapping("/owner")
	Owner newOwner(@RequestBody Owner newOwner) {
	    return ownerService.create(newOwner);
	 }
	
	@PutMapping("/owner")
	Owner updateOwner(@RequestBody Owner newOwner) {
	    return ownerService.update(newOwner);
	 }
	
	
}
