package com.tecsup.petclinic.exception;

public class VetsNotFoundException extends Exception{
	
private static final long serialVersionUID = 1L;
	
	public VetsNotFoundException(String message) {
		super(message);
	}
	
}
