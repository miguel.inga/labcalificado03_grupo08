package com.tecsup.petclinic.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.tecsup.petclinic.domain.Vets;
import com.tecsup.petclinic.exception.VetsNotFoundException;

@SpringBootTest
@RunWith(SpringRunner.class)
@AutoConfigureTestDatabase(replace = Replace.NONE)
public class VetsServiceTest {

	private static final Logger logger = LoggerFactory.getLogger(OwnerServiceTest.class);
	
	@Autowired
	private VetsService vetsService;
	
	@Test
	public void testFindVetById() {
		long ID=1;
		String NAME="James";
		Vets vet=null;
		try {
			vet=vetsService.findById(ID);
		}catch(VetsNotFoundException e) {
			fail(e.getMessage());
		}

		assertEquals(NAME,vet.getFirstName());
	}
	
	@Test
	public void testCreateVet() {
		String FIRST_NAME="Chichico";
		String LAST_NAME="Quito";
		
		Vets vets = new Vets(FIRST_NAME,LAST_NAME);
		vets=vetsService.create(vets);
		logger.info("" + vets);
		
		assertThat(vets.getId()).isNotNull();
		assertEquals(FIRST_NAME, vets.getFirstName());
		assertEquals(LAST_NAME, vets.getLastName());
	}
	
	@Test
	public void testUpdatePet() {
		String FIRST_NAME="Miguel";
		String LAST_NAME="Inga";
		long create_id = -1;
		
		String UP_FIRST_NAME="Lee";
		String UP_LAST_NAME="Tipismana";
		
		Vets vet = new Vets(FIRST_NAME, LAST_NAME);
		
		Vets readVet=vetsService.create(vet);
		
		create_id = readVet.getId();
		
		readVet.setFirstName(UP_FIRST_NAME);
		readVet.setLastName(UP_LAST_NAME);
		
		Vets upgrateVet=vetsService.update(readVet);
		
		assertThat(create_id).isNotNull();
		assertEquals(UP_FIRST_NAME, upgrateVet.getFirstName());
		assertEquals(UP_LAST_NAME, upgrateVet.getLastName());
	}
	
	@Test
	public void testDeletePet() {
		String FIRST_NAME="Ale";
		String LAST_NAME="Inga";
		
		Vets vet = new Vets(FIRST_NAME, LAST_NAME);
		vet=vetsService.create(vet);
		
		try {
			vetsService.delete(vet.getId());
		}catch(VetsNotFoundException e) {
			fail(e.getMessage());
		}
		
		try {
			vetsService.findById(vet.getId());
			assertTrue(false);
		}catch(VetsNotFoundException e) {
			assertTrue(true);
		}
	}
}

