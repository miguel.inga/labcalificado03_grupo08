package com.tecsup.petclinic.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.tecsup.petclinic.domain.Owner;
import com.tecsup.petclinic.exception.OwnerNotFoundException;

@SpringBootTest
@RunWith(SpringRunner.class)
@AutoConfigureTestDatabase(replace = Replace.NONE)
public class OwnerServiceTest {

	private static final Logger logger = LoggerFactory.getLogger(OwnerServiceTest.class);

	@Autowired
	private OwnerService ownerService;

	/**
	 * 
	 */
	
	@Test
	public void testCreateOwner() {
		String FIRST_NAME="Chichico";
		String LAST_NAME="Quito";
		String ADDRESS="Av. los macacos";
		String CITY="Lima";
		String TELEPHONE="987654321";
		
		Owner owner = new Owner(FIRST_NAME,LAST_NAME,ADDRESS,CITY,TELEPHONE);
		owner=ownerService.create(owner);
		logger.info("" + owner);
		
		assertThat(owner.getId()).isNotNull();
		assertEquals(FIRST_NAME, owner.getFirstName());
		assertEquals(LAST_NAME, owner.getLastName());
		assertEquals(ADDRESS, owner.getAddress());
		assertEquals(CITY, owner.getCity());
		assertEquals(TELEPHONE, owner.getTelephone());
	}
	
	@Test
	public void testDeleteOwner() {

		String FIRST_NAME="Chichico";
		String LAST_NAME="Quito";
		String ADDRESS="Av. los macacos";
		String CITY="Lima";
		String TELEPHONE="987654321";

		Owner owner = new Owner(FIRST_NAME,LAST_NAME,ADDRESS,CITY,TELEPHONE);
		owner=ownerService.create(owner);
		logger.info("" + owner);

		try {
			ownerService.delete(owner.getId());
		} catch (OwnerNotFoundException e) {
			fail(e.getMessage());
		}
			
		try {
			ownerService.findById(owner.getId());
			assertTrue(false);
		} catch (OwnerNotFoundException e) {
			assertTrue(true);
		} 				

	}
	
	@Test
	public void testUpdateOwner() {

		String FIRST_NAME="Chichico";
		String LAST_NAME="Quito";
		String ADDRESS="Av. los macacos";
		String CITY="Lima";
		String TELEPHONE="987654321";
		long create_id = -1;

		String UP_FIRST_NAME="Chichico";
		String UP_LAST_NAME="Quito";
		String UP_ADDRESS="Av. los macacos";
		String UP_CITY="Lima";
		String UP_TELEPHONE="987654321";
		
		System.out.println("gogogoo+++++++++++++++");

		Owner owner = new Owner(FIRST_NAME,LAST_NAME,ADDRESS,CITY,TELEPHONE);

		// Create record
		logger.info(">" + owner);
		Owner readOwner = ownerService.create(owner);
		logger.info(">>" + readOwner);

		create_id = readOwner.getId();

		// Prepare data for update
		readOwner.setFirstName(UP_FIRST_NAME);
		readOwner.setLastName(UP_LAST_NAME);
		readOwner.setAddress(UP_ADDRESS);
		readOwner.setCity(UP_CITY);
		readOwner.setTelephone(UP_TELEPHONE);

		// Execute update
		Owner upgradePet = ownerService.update(readOwner);
		logger.info(">>>>" + upgradePet);

		assertThat(create_id).isNotNull();
		assertEquals(create_id, upgradePet.getId());
		assertEquals(FIRST_NAME, upgradePet.getFirstName());
		assertEquals(LAST_NAME, upgradePet.getLastName());
		assertEquals(ADDRESS, upgradePet.getAddress());
		assertEquals(CITY, upgradePet.getCity());
		assertEquals(TELEPHONE, upgradePet.getTelephone());
	}


}
